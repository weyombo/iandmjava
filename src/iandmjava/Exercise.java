/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iandmjava;

import java.util.function.Function;

/**
 *
 * @author Glen Weyombo
 */
public class Exercise {

    public  Boolean isPrimes(Integer num) {
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    Function<Integer, Boolean> f = this::isPrimes;
    Function<Integer, Boolean> g = Memoizer.memoize(f);


    public  Boolean memoizationExample(Integer num) {
        Boolean result1 = g.apply(num);
        return result1;
    }
}
